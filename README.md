# PasswordHasher

## What does it do?

The script takes an input file, then hashes every line in that file with the chosen algorithm and then saves all these hashed lines in a single output file.

## What do I use it for?

I'm using it to quickly generate hashed password files to test different hashing algorithms with Hashcat/John the Ripper.

## Installation

If you're planning on using bcrypt you need to install the dependencies with `pip install -r requirements.txt`
