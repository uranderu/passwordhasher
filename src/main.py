import hashlib


class Hasher:
    def read_passwords(self):
        input_path = input(
            "Please specify the path of the file which lines are to be hashed.\n"
        )

        with open(input_path) as file:
            lines = file.readlines()

        # Strips the newline character
        self.passwords = [item.strip() for item in lines]

        print("Passwords successfully read.\n")

    def hash_passwords(self):
        chosen_algorithm = int(
            input(
                "Make a choice out of the following algorithms:\n 0: MD5\n 1: SHA512\n 2: PBKDF2-HMAC-SHA512\n 3: bcrypt\n"
            )
        )

        if chosen_algorithm == 0:
            for password in self.passwords:
                hashed_password = hashlib.md5(password.encode()).hexdigest()
                self.hashed_passwords.append(f"{hashed_password}\n")
        elif chosen_algorithm == 1:
            for password in self.passwords:
                hashed_password = hashlib.sha512(password.encode()).hexdigest()
                self.hashed_passwords.append(f"{hashed_password}\n")
        elif chosen_algorithm == 2:
            import random

            iterations = 120000  # OWASP 2021 recommendation

            for password in self.passwords:
                hashed_password = hashlib.pbkdf2_hmac(
                    "sha512",
                    password.encode(),
                    random.getrandbits(128).to_bytes(16, "little"),
                    iterations,
                ).hex()

                self.hashed_passwords.append(f"{hashed_password}\n")
        elif chosen_algorithm == 3:
            import bcrypt

            for password in self.passwords:
                hashed_password = bcrypt.hashpw(
                    password.encode(), bcrypt.gensalt()
                ).decode()
                self.hashed_passwords.append(f"{hashed_password}\n")

        else:
            self.hash_passwords()

        print("Passwords hashed successfully.\n")

    def save_passwords(self):
        output_path = input("Please specify the output path.\n")

        with open(output_path, "w") as file:
            file.writelines(self.hashed_passwords)

        print("File saved successfully.\n")

    def __init__(self):
        self.passwords = None
        self.hashed_passwords = []

        self.read_passwords()
        self.hash_passwords()
        self.save_passwords()


if __name__ == "__main__":
    Hasher()
